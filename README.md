# Pipeline statistics generator

Provide high-level insight into pipeline metrics and CI configuration attributes in order to pinpoint slow or otherwise problematic pipelines.

The script crawls finished pipelines and CI yml content in all specified projects and generates a number of statistics.

Statistics provided:
* AVG (min, max) pipeline execution times
  * why interesting: Allows you to focus on long-running pipelines which will have the greatest optimization potential
* AVG pipeline queue times
  * why interesting: Allows you to examine the runner infrastructure to make sure runners can effectively provisioned for the projects
* AVG minutes between pipeline executions
  * why interesting: Allows you to focus high frequency pipelines which consume more resources, thus should be optimized
* pipeline success, failure and cancelled percentage
  * why interesting: Brittle pipelines are an efficiency problem for developers, thus these require more attention
* count of jobs with scripts
  * why interesting: A large number of jobs indicates a more complex pipeline, thus more important to optimize
* list of images used
  * why interesting: Some images may be outdated, very large or present a security issue
* count of jobs with artifacts
  * why interesting: More artifact generating jobs mean more traffic and storage consumption. Artifact size can unfortunately not yet be evaluated with this tool
* count of jobs with cache
  * why interesting: Caching can speed up pipelines and the absence of caching may indicate a non-optimized pipeline
* count of multi-project pipelines
* count of dynamic child pipelines
  * why interesting: Multi-project or dynamic child pipelines are advanced features whose usage may indicate more complex pipelines in need of optimization
* count of job rules
  * why interesting: Job rules should be used in jobs to limit when they are run. An absence of job rules indicates optimization potential in a pipeline
* count of workflow rules
  * why interesting: Workflow rules should be used to only run pipelines in specific situation, for example in Merge Requests and Main, instead of each commit. An absence of workflow rules indicates optimization potential in a pipeline

## Usage

### Report format

* **project_ci_data.csv**: A `Tab (\t)` separated CSV containing a list of projects with all statistics per project. See an example file [here](./example_report.tsv).
* **project_ci_data.json**: A JSON representation of above CSV
* **ci_raw_paths.json**: A JSON list of projects with object paths extracted from the CI configuration

### Installing locally or on self-hosted GitLab

`pip3 install -r requirements.txt`

### Running the report

`python3 find_ci_criteria.py $GIT_TOKEN $CONFIG_YML --gitlab $GITLAB_URL`

* $GIT_TOKEN: An access token with **API and read repository** permissions for the configured groups and projects
* $CONFIG_YML: Path to the `config.yml` file containing the configuration
* $GITLAB_URL: Optional URL of the GitLab instance. Defaults to `https://gitlab.com`

### Number of API calls

Gather projects:
Projects: Number of projects in group / 100 (pagesize)
Gather CI data:
+ Pipelines: Number of projects (get one page of pipelines. Setting `pipeline_count` above 100 will add another call per project for each 100 pipelines)
+ Pipeline details: Number of projects * Number of pipelines (get details for each pipeline individually. Configured via `pipeline_count`.)
+ CI configuration data:  Number of projects (get CI configuration for each project and parse it)

So for a group with 5 projects and `pipeline_count` = 20, we are getting:
* Projects: 1 call to group projects = 5 projects
* Pipelines: 5 calls to project pipelines for each project = 5 pipelines calls resulting in 5*20=100 pipeline objects
* Pipeline details: 100 calls to pipeline details
* CI config: 5 calls to project CI configuration
= 1+5+100+5 = 111 calls

## Configuration

### Define groups or projects

- edit config.yml
  - specify `groups` or `projects` as arrays using their ID
  - If groups are used, all projects in the group are retrieved instead of individual projects.
  - If neither are defined, all projects the token has access to will be used. On self-managed with an admin token, that's equivalent to all projects on the instance

### Further configuration options

* pipeline_source: Limit which pipelines are retrieved. One of: push, web, trigger, schedule, api, external, pipeline, chat, webide, merge_request_event, external_pull_request_event, parent_pipeline, ondemand_dast_scan, or ondemand_dast_validation
* pipeline_count: The number of pipelines to use for statistics. Because we need to [call the API for every single pipeline object](https://docs.gitlab.com/ee/api/pipelines.html#get-a-single-pipeline) we want to get stats from, this drives the number of API calls per project

## DISCLAIMER

This script is provided **for educational purposes only**. It is not supported by GitLab. You can create an issue or contribute via MR if you encounter any problems.
