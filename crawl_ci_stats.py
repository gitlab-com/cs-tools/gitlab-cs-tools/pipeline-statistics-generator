#!/usr/bin/env python3

import gitlab
import json
import argparse
import yaml
import csv
import re
from datetime import datetime, timedelta

class Pipeline_Crawler():

    gitlab_url = "https://gitlab.com/"
    gl = None
    groups = []
    projects = []
    pipeline_count = ""

    def __init__(self, args):
        configfile = args.configfile
        self.gl = gitlab.Gitlab(args.gitlab, private_token=args.token, retry_transient_errors=True, per_page=100)
        self.read_config(configfile)

    def read_config(self, configfile):
        with open(configfile, "r") as c:
            config = yaml.load(c, Loader=yaml.FullLoader)
            if "groups" in config:
                self.groups = config["groups"]
                self.projects = self.get_group_projects(self.groups)
            else:
                if not "projects" in config:
                    print("Retrieving all projects the token has access to")
                    self.projects = self.gl.projects.list(iterator=True)
                else:
                    for project in config["projects"]:
                        self.projects.append(self.gl.projects.get(project))
            if "pipeline_source" in config:
                self.pipeline_source = config["pipeline_source"]
            if "pipeline_count" in config:
                self.pipeline_count = config["pipeline_count"]

    # get all projects for a group
    def get_group_projects(self, groups):
        projects = []
        for topgroup in groups:
            print("[INFO] Getting projects for group %s" % topgroup)
            group = self.gl.groups.get(topgroup)
            group_projects = group.projects.list(iterator=True, include_subgroups=True, lazy=True)
            for group_project in group_projects:
                projects.append(group_project)
        print("[INFO] Found %s projects" % len(projects))
        return projects

    def get_pipelines(self, project):
        pipeline_list = []
        pipelines = []
        project_object = self.gl.projects.get(project.id, lazy=True)
        try:
            if self.pipeline_source:
                pipelines = project_object.pipelines.list(iterator=True, scope="finished", source=self.pipeline_source)
            else:
                pipelines = project_object.pipelines.list(iterator=True, scope="finished")
        except Exception as err:
            if self.pipeline_source:
                print("[WARN] No finished %s pipelines found for project %s" % (self.pipeline_source, project.path_with_namespace))
            else:
                print("[WARN] No finished pipelines found for project %s" % (project.path_with_namespace))
            print(err)
            return pipelines

        #n+1 query to get sufficient metadata, may want to switch to graphql
        for pipeline in pipelines:
            pipeline_details = project_object.pipelines.get(pipeline.id)
            pipeline_list.append(pipeline_details)
            if len(pipeline_list) == self.pipeline_count:
                break

        return pipeline_list

    def get_ci_metadata(self, project):
        has_ci = False
        parsed_ci = {}
        merged_yml = self.get_merged_yml(project)
        if merged_yml:
            parsed_ci = yaml.safe_load(merged_yml)
        else:
            has_ci = False
        pathlist = set()
        if parsed_ci:
            try:
                pathlist = self.make_path_list(parsed_ci, pathlist)
                has_ci = True
            except Exception as e:
                print("[ERROR] Could not parse CI for project %s" % (project.path_with_namespace))
                has_ci = False
                print(e)
        project_data = {}
        project_data["name"] = project.name
        project_data["path"] = project.attributes["path_with_namespace"]
        project_data["url"] = project.attributes["web_url"]
        project_data["has_ci"] = has_ci
        project_paths = {}
        project_paths["name"] = project.name
        project_paths["path"] = project.attributes["path_with_namespace"]
        project_paths["url"] = project.attributes["web_url"]
        project_paths["paths"] = sorted(list(pathlist))
        return project_data, project_paths

    def make_path_list(self, json, path_list = set(), path = ""):
        if isinstance(json, dict):
            for key, value in json.items():
                new_path = f"{path}:{key}" if path else key
                self.make_path_list(value, path_list, new_path )
        elif isinstance(json, list):
            for index, value in enumerate(json):
                new_path = f"{path}:{index}" if path else str(index)
                self.make_path_list(value, path_list, new_path)
        else:
            path_list.add(path + ":" + str(json))
        return path_list

    # getting the merged project CI yml
    def get_merged_yml(self, project):
        try:
            project_object = self.gl.projects.get(project.id, lazy=True)
            lint_result = project_object.ci_lint.get()
            if lint_result.valid == True:
                return lint_result.merged_yaml
            else:
                print("YML invalid or missing")
                return None
        except Exception as err:
            print("[WARN] Could not lint CI file:")
            print(err)
            return None
        
    def compute_pipeline_stats(self, pipelines):
        stats = {"avg_duration":0,"avg_queued_duration":0,"avg_minutes_between_pipelines":0,"success_percentage":0,"failed_percentage":0,"canceled_percentage":0,"max_duration":0,"min_duration":0}
        if not pipelines:
            return stats
        last_started = None
        for pipeline in pipelines:
            if pipeline.duration is None:
                pipeline.duration = 0
            stats["avg_duration"] += pipeline.duration

            if pipeline.queued_duration is None:
                pipeline.queued_duration = 0
            stats["avg_queued_duration"] += pipeline.queued_duration

            if last_started is None:
                last_started = pipeline.created_at
            else:
                difference = datetime.fromisoformat(last_started) - datetime.fromisoformat(pipeline.created_at)
                difference = difference / timedelta(minutes=1)
                stats["avg_minutes_between_pipelines"] += difference

            if pipeline.status == "success":
                stats["success_percentage"] += 1
            elif pipeline.status == "failed":
                stats["failed_percentage"] += 1
            elif pipeline.status == "canceled":
                stats["canceled_percentage"] += 1
            stats["max_duration"] = max(stats["max_duration"], pipeline.duration)
            if stats["min_duration"] == 0:
                stats["min_duration"] = pipeline.duration
            else:
                stats["min_duration"] = min(stats["min_duration"], pipeline.duration)
        stats["avg_duration"] /= len(pipelines)
        stats["avg_queued_duration"] /= len(pipelines)
        stats["avg_minutes_between_pipelines"] /= len(pipelines)
        stats["success_percentage"] /= len(pipelines)
        stats["success_percentage"] *= 100
        stats["failed_percentage"] /= len(pipelines)
        stats["failed_percentage"] *= 100
        stats["canceled_percentage"] /= len(pipelines)
        stats["canceled_percentage"] *= 100
        return stats
    
    def compute_yml_stats(self, paths_dict):
        stats = {"job_script_count":0,"images":[],"job_artifact_count":0,"cache_count":0,"multi_project_pipeline_count":0,"dynamic_pipeline_count":0,"workflow_rules_count":0,"job_rules_count":0}
        paths_string = ",".join(paths_dict["paths"])
        stats["job_script_count"] = len(re.findall(":script:0", paths_string))
        #use regex to find images and add all to a list
        images = re.findall(":image:.*?,", paths_string)
        image_set = set()
        for image in images:
            image = image.replace(":image:","")
            image = image.replace(" ,","").strip()
            image_set.add(image)
        default_image = re.findall("^image:.*?,", paths_string)
        default_image.extend(re.findall(",image:.*?,", paths_string))
        for image in default_image:
            image = image.replace("image:","")
            image = image.replace(",","").strip()
            image_set.add(image)
        stats["images"] = list(image_set)

        artifacts = re.findall(":artifacts:paths:0", paths_string)
        stats["job_artifact_count"] = len(artifacts)

        child_pipelines = re.findall(":trigger.*:project:.*", paths_string)
        stats["multi_project_pipeline_count"] = len(child_pipelines)

        dynamic_pipelines = re.findall(":trigger.*:artifact:.*", paths_string)
        stats["dynamic_pipeline_count"] = len(dynamic_pipelines)

        cache_keys = re.findall(":cache:0:key", paths_string)
        stats["cache_count"] = len(cache_keys)

        rules = re.finditer(",[a-zA-Z0-9 :]*?:rules:[0-9]+:(if|changes):", paths_string)
        ruleset = set()
        for rule in rules:
            if rule.group().startswith(",workflow:"):
                stats["workflow_rules_count"] += 1
            else:
                ruleset.add(rule.group())
        stats["job_rules_count"] = len(ruleset)
        return stats

parser = argparse.ArgumentParser(description='Create report for GitLab issues')
parser.add_argument('token', help='API token able to read the requested projects')
parser.add_argument('configfile', help='YML file that defines requested groups, projects and adoption parameters')
parser.add_argument('--gitlab', help='URL of the gitlab instance', default="https://gitlab.com/")

args = parser.parse_args()
crawler = Pipeline_Crawler(args)
pathlists = []
projectdata = []
for project in crawler.projects:
    print("[INFO] Pulling pipeline data for project %s" % project.path_with_namespace)
    metadata = crawler.get_ci_metadata(project)
    pipelines = crawler.get_pipelines(project)
    pipeline_stats = crawler.compute_pipeline_stats(pipelines)
    metadata[0].update(pipeline_stats)
    ci_yml_stats = crawler.compute_yml_stats(metadata[1])
    metadata[0].update(ci_yml_stats)
    projectdata.append(metadata[0])
    pathlists.append(metadata[1])

with open("project_ci_data.json","w") as ci_json:
    json.dump(projectdata, ci_json, indent=2)

with open("ci_raw_paths.json","w") as ci_paths_json:
    json.dump(pathlists, ci_paths_json, indent=2)

with open("project_ci_data.csv","w") as ci_csv:
    writer = csv.writer(ci_csv, delimiter="\t")
    header = projectdata[0].keys()
    writer.writerow(header)
    for project in projectdata:
        row = project.values()
        writer.writerow(row)